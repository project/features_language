<?php
/**
 * Drush file.
 */

function _drush_features_language_set_default_language() {
  global $language;

  $default_language = _features_language_get_default_language();

  $language = _features_language_set_language_to_default_specified($default_language);

  drush_log(FEATURES_LANGUAGE_NAME . ' has switched the feature drush command
   to the correct language: ' . $language->language, 'warning');
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_language_pre_features_update() {
  _drush_features_language_set_default_language();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_language_pre_features_update_all() {
  _drush_features_language_set_default_language();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_language_pre_features_export() {
  _drush_features_language_set_default_language();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_language_pre_features_diff() {
  _drush_features_language_set_default_language();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_language_pre_features_list() {
  _drush_features_language_set_default_language();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_language_pre_features_revert() {
  _drush_features_language_set_default_language();
}

/**
 * Implements drush_hook_pre_COMMAND()
 */
function drush_features_language_pre_features_revert_all() {
  _drush_features_language_set_default_language();
}
